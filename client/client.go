package main

import (
	"context"
	"google.golang.org/grpc"
	"jrpc/pb"
	"log"
	"time"
)

func main() {
	conn, err := grpc.Dial("localhost:6000", grpc.WithInsecure())
	if err != nil {
		log.Fatalf("failed to connect: %s", err)
	}
	defer conn.Close()

	client := mypackage.NewSimpleServiceClient(conn)
	stream, err := client.SimpleRPC(context.Background())
	waitc := make(chan struct{})
	msg := &mypackage.SimpleData{}
	msg.Msg = "sup"

	go func() {
		for {
			log.Println("Sleeping...")
			time.Sleep(2 * time.Second)
			log.Println("Sending msg...")
			err = stream.Send(msg)
			if err != nil {
				log.Println("Error::", err)
			}
		}
	}()
	<-waitc
	stream.CloseSend()
}
